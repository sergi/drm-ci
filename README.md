# DRM CI

CI-related files for the drm tree

## Getting started

In your kernel repo's configuration (eg. https://gitlab.freedesktop.org/tomeu/linux/-/settings/ci_cd), specify a URL pointing to the .gitlab-ci.yml file in this repo (eg. https://gitlab.freedesktop.org/gfx-ci/drm-ci/-/raw/main/.gitlab-ci.yml).

